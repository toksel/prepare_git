﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using Winforms = System.Windows.Forms;
using System.Threading;
using System.Security.Principal;

namespace PrepareGit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {


            //если не нужны права администратора то запускаем сразу инициализацию
            InitializeComponent();

            //загружаем почту, путьк гит и путь к репозиторию из конфига
            string configFileName = "config.tlsm";

            txtbox_git_email.Text = Prog.GetConfig(configFileName, "bitbucketAccountEmail");

            txtbox_path.Text = Prog.GetConfig(configFileName, "repoPath");

            txtbox_path_git_instal.Text = Prog.GetConfig(configFileName, "gitPath");

            string pathGitSearch = txtbox_path_git_instal.Text;

            string gitPath = Prog.ReturnPath(pathGitSearch, "ssh-agent.exe", "ssh-keygen.exe", 3);
            if (gitPath.Length > 0)
            {
                TextValues.textPhase1 = $"1. Git обнаружен по пути {gitPath}";
                TextValues.pathToSSHKeygen = Prog.ReturnPath(pathGitSearch, "ssh-agent.exe", "ssh-keygen.exe", 1);
                txtbox_path_git_instal.Text = gitPath;
            }
            else
            {
                TextValues.textPhase1 = $"1. Git не обнаружен в {pathGitSearch}\nНеобходимо указать путь к Git или установить его";
            }


            string pathSSHKeySearch = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\.ssh";
            string SSHPath = Prog.ReturnPath(pathSSHKeySearch, "id_rsa.pub", "id_rsa", 1);
            if (SSHPath.Length > 0)
            {
                FileStream file = new FileStream(SSHPath + @"\id_rsa.pub", FileMode.Open);
                StreamReader reader = new StreamReader(file);
                string publicKey = reader.ReadToEnd();
                reader.Close();

                if (publicKey.Contains(txtbox_git_email.Text))
                {
                    TextValues.textPhase2 = $"2. SSH-Key обнаружен по пути {SSHPath}\nПубличный ключ:\n{publicKey}";
                }
                else
                {
                    TextValues.textPhase2 = $"2. SSH-Key обнаружен по пути {SSHPath}, но он не подходит";
                }

            }
            else
            {
                TextValues.textPhase2 = $"2. SSH-Key не обнаружен в {pathSSHKeySearch}\nНеобходимо сгенерировать SSH-Key";
            }

            string pathGitClone = txtbox_path.Text;
            if (Directory.Exists(pathGitClone))
            {
                if ((Directory.GetDirectories(txtbox_path.Text).Length + Directory.GetFiles(txtbox_path.Text).Length) > 0)
                {
                    TextValues.textPhase3 = $"3. Папка {pathGitClone} существует и не пуста.\nВозможно акутализировать содержимое.";
                }
                else
                {
                    TextValues.textPhase3 = $"3. Папка {pathGitClone} существует и пуста.\nВозможно скачать с репозитория.";
                }
            }
            else
            {
                TextValues.textPhase3 = $"3. Папки {pathGitClone} не существует.\nВыберите другую папку.";
            }

            UpdateStatusAllPhases();



            //// если нужно запускать от имени администратора
            //WindowsPrincipal principal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            //if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
            //{
            //    ProcessStartInfo startInfo = new ProcessStartInfo();
            //    startInfo.Verb = "runas";
            //    startInfo.FileName = Winforms.Application.ExecutablePath;
            //    try
            //    {
            //        Process.Start(startInfo);
            //    }
            //    catch (System.ComponentModel.Win32Exception)
            //    {

            //    }
            //    this.Close();
            //}
            //else
            //{
            //    InitializeComponent();

            //    //загружаем почту, путьк гит и путь к репозиторию из конфига
            //    string configFileName = "config.tlsm";

            //    txtbox_git_email.Text = Prog.GetConfig(configFileName, "bitbucketAccountEmail");

            //    txtbox_path.Text = Prog.GetConfig(configFileName, "repoPath");

            //    txtbox_path_git_instal.Text = Prog.GetConfig(configFileName, "gitPath");

            //    string pathGitSearch = txtbox_path_git_instal.Text;

            //    string gitPath = Prog.ReturnPath(pathGitSearch, "ssh-agent.exe", "ssh-keygen.exe", 3);
            //    if (gitPath.Length > 0)
            //    {
            //        TextValues.textPhase1 = $"1. Git обнаружен по пути {gitPath}";
            //        TextValues.pathToSSHKeygen = Prog.ReturnPath(pathGitSearch, "ssh-agent.exe", "ssh-keygen.exe", 1);
            //        txtbox_path_git_instal.Text = gitPath;
            //    }
            //    else
            //    {
            //        TextValues.textPhase1 = $"1. Git не обнаружен в {pathGitSearch}\nНеобходимо указать путь к Git или установить его";
            //    }


            //    string pathSSHKeySearch = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\.ssh";
            //    string SSHPath = Prog.ReturnPath(pathSSHKeySearch, "id_rsa.pub", "id_rsa", 1);
            //    if (SSHPath.Length > 0)
            //    {
            //        FileStream file = new FileStream(SSHPath + @"\id_rsa.pub", FileMode.Open);
            //        StreamReader reader = new StreamReader(file);
            //        string publicKey = reader.ReadToEnd();
            //        reader.Close();

            //        if (publicKey.Contains(txtbox_git_email.Text))
            //        {
            //            TextValues.textPhase2 = $"2. SSH-Key обнаружен по пути {SSHPath}\nПубличный ключ:\n{publicKey}";
            //        }
            //        else
            //        {
            //            TextValues.textPhase2 = $"2. SSH-Key обнаружен по пути {SSHPath}, но он не подходит";
            //        }

            //    }
            //    else
            //    {
            //        TextValues.textPhase2 = $"2. SSH-Key не обнаружен в {pathSSHKeySearch}\nНеобходимо сгенерировать SSH-Key";
            //    }

            //    string pathGitClone = txtbox_path.Text;
            //    if (Directory.Exists(pathGitClone))
            //    {
            //        if ((Directory.GetDirectories(txtbox_path.Text).Length + Directory.GetFiles(txtbox_path.Text).Length) > 0)
            //        {
            //            TextValues.textPhase3 = $"3. Папка {pathGitClone} существует и не пуста.\nВозможно акутализировать содержимое.";
            //        }
            //        else
            //        {
            //            TextValues.textPhase3 = $"3. Папка {pathGitClone} существует и пуста.\nВозможно скачать с репозитория.";
            //        }
            //    }
            //    else
            //    {
            //        TextValues.textPhase3 = $"3. Папки {pathGitClone} не существует.\nВыберите другую папку.";
            //    }

            //    UpdateStatusAllPhases();
            //}





        }

        private void btn_path_Button_Click(object sender, RoutedEventArgs e)
        {
            txtbox_path.Text = Prog.SelectFolder();

            string pathGitClone = txtbox_path.Text;
            if (Directory.Exists(pathGitClone))
            {
                if ((Directory.GetDirectories(txtbox_path.Text).Length + Directory.GetFiles(txtbox_path.Text).Length) > 0)
                {
                    TextValues.textPhase3 = $"3. Папка {pathGitClone} существует и не пуста.\nВозможно акутализировать содержимое.";
                }
                else
                {
                    TextValues.textPhase3 = $"3. Папка {pathGitClone} существует и пуста.\nВозможно скачать с репозитория.";
                }
                Prog.UdpateUserConfig("config.tlsm", txtbox_path_git_instal.Text, txtbox_git_email.Text, txtbox_path.Text);
            }
            else
            {
                TextValues.textPhase3 = $"3. Папки {pathGitClone} не существует.\nВыбирите другую папку.";
            }
            UpdateStatusAllPhases();
        }

        private void btn_RunBAT_GitClone_Button_Click(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(txtbox_path.Text))
            {
                Winforms.MessageBox.Show($"Папку {txtbox_path.Text} не удалось обноружить, выберите другую", "Требуется выбрать папку");

                txtbox_path.Text = Prog.SelectFolder();
            }

            string gitName = "git@bitbucket.org:toksel/online-test-branches.git";


            if (Directory.Exists(txtbox_path.Text))
            {
                //List<string> com = new List<string>();
                //com.Add($"set directory={txtbox_path.Text}");
                ////com.Add(@"if exist ""directory\*.*"" (");
                //com.Add(@"if exist ""%directory%\*.*"" (");
                ////если папка есть, то пишем, что папка есть
                //com.Add("echo folder exists");
                ////проверяем есть ли в папке что-нибудь
                //com.Add(@"dir ""%directory%"" /a-d >nul 2>nul");
                //com.Add("if errorlevel 1 (");
                ////если папка не пустая
                //com.Add("echo folder contains files");
                //com.Add(@"cd ""%directory%""");
                //com.Add("git init");
                ////com.Add($"git remote add origin {gitName}");
                //com.Add($"git pull {gitName}");
                //com.Add(") else (");
                ////если папка пустая
                //com.Add("echo folder is empty");
                ////com.Add("git init");
                ////com.Add($"git remote add -t \\* -f origin {gitName}");
                ////com.Add($"git fetch --all {gitName}");
                ////com.Add($"git reset --hard origin/master");
                //com.Add(@"cd ""%directory%""");
                //com.Add("git init");
                ////com.Add($"git remote add origin {gitName}");               
                //com.Add($"git clone {gitName}");
                //com.Add(")");
                //com.Add(") else (");
                ////если папки нет, то к концу файла
                //com.Add("echo folder does not exist");
                //com.Add("goto The_End");
                //com.Add(")");
                //com.Add(":The_End");
                //com.Add("pause");


                List<string> com = new List<string>();
                com.Add(@"set directory=""" + txtbox_path.Text + @"""");
                com.Add(@"if exist ""%directory%\*.*"" (");
                com.Add("echo folder exists");
                com.Add(@"cd /d ""%directory%""");
                com.Add("git init");
                com.Add($"git pull {gitName}");
                com.Add(") else (");
                com.Add("echo folder does not exist");
                com.Add(")");
                //com.Add("pause");

                //если в пути размещения bat файла есть пробелы, то cmd выдаст ошибку на старте, придется запускать bat вручную
                if (Winforms.Application.StartupPath.ToString().Contains(" "))
                {
                    Cmd.RunCMD(Winforms.Application.StartupPath.ToString(), "Git-Pull-Clone", com, true);
                }
                else
                {
                    Cmd.RunCMD(Winforms.Application.StartupPath.ToString(), "Git-Pull-Clone", com, false);
                }
            }
        }

        private void btn_install_git_Button_Click(object sender, RoutedEventArgs e)
        {
            string gitInstallLocation = Winforms.Application.StartupPath.ToString();
            if (OSBitCheck.Is64())
            {
                gitInstallLocation += @"\add\Git-2.21.0-64-bit.exe";
            }
            else
            {
                gitInstallLocation += @"\add\Git-2.21.0-32-bit.exe";
            }
            
            Process.Start(gitInstallLocation);
        }

        private void btn_RunBAT_SSHKey_Button_Click(object sender, RoutedEventArgs e)
        {
            ////добавляем пусть к ssh-keygen в %PATH%
            //if (TextValues.pathToSSHKeygen != null)
            //{
            //    if (TextValues.pathToSSHKeygen.Length > 0 && !Environment.GetEnvironmentVariable("PATH").Contains(TextValues.pathToSSHKeygen))
            //    {
            //        string pathOld = Environment.GetEnvironmentVariable("PATH");
            //        Environment.SetEnvironmentVariable("PATH", pathOld + $";{TextValues.pathToSSHKeygen}", EnvironmentVariableTarget.Machine);

            //        Winforms.MessageBox.Show($"Путь {TextValues.pathToSSHKeygen} внесен в системную переменную %PATH%, требуется рестарт программы", "Требуется перезапуск");

            //        ProcessStartInfo startInfo = new ProcessStartInfo();
            //        startInfo.Verb = "runas";
            //        startInfo.FileName = Winforms.Application.ExecutablePath;
            //        try
            //        {
            //            Process.Start(startInfo);
            //        }
            //        catch (System.ComponentModel.Win32Exception)
            //        {

            //        }
            //        this.Close();
            //    }
            //}

            if (TextValues.pathToSSHKeygen != null)
            {
                string pathToSSHKeygenFull = TextValues.pathToSSHKeygen + @"\ssh-keygen.exe";

                if (File.Exists(pathToSSHKeygenFull))
                {
                    List<string> com = new List<string>();
                    com.Add(@"""" + pathToSSHKeygenFull + @""" -f """ + System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile) + @"\.ssh\id_rsa"" -t rsa -b 4096 -C """ + txtbox_git_email.Text + @""" -P """"");
                    //com.Add("pause");
                    if (Winforms.Application.StartupPath.ToString().Contains(" "))
                    {
                        Cmd.RunCMD(Winforms.Application.StartupPath.ToString(), "GetSSH", com, true);
                    }
                    else
                    {
                        Cmd.RunCMD(Winforms.Application.StartupPath.ToString(), "GetSSH", com, false);
                    }
                }

                //Thread.Sleep(3000);

                string pathSSHKeySearch = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\.ssh";
                string SSHPath = Prog.ReturnPath(pathSSHKeySearch, "id_rsa.pub", "id_rsa", 1);
                if (SSHPath.Length > 0)
                {
                    FileStream file = new FileStream(SSHPath + @"\id_rsa.pub", FileMode.Open);
                    StreamReader reader = new StreamReader(file);
                    string publicKey = reader.ReadToEnd();
                    reader.Close();

                    if (publicKey.Contains(txtbox_git_email.Text))
                    {
                        TextValues.textPhase2 = $"2. SSH-Key обнаружен по пути {SSHPath}\nПубличный ключ:\n{publicKey}";
                    }
                    else
                    {
                        TextValues.textPhase2 = $"2. SSH-Key обнаружен по пути {SSHPath}, но он не подходит";
                    }

                }
                else
                {
                    TextValues.textPhase2 = $"2. SSH-Key не обнаружен в {pathSSHKeySearch}\nНеобходимо сгенерировать SSH-Key";
                }

            }


            UpdateStatusAllPhases();
        }

        private void btn_path_to_git_Button_Click(object sender, RoutedEventArgs e)
        {
            txtbox_path_git_instal.Text = Prog.SelectFolder();

            string pathGitSearch = txtbox_path_git_instal.Text;
            string gitPath = Prog.ReturnPath(pathGitSearch, "ssh-agent.exe", "ssh-keygen.exe", 3);
            if (gitPath.Length > 0)
            {
                TextValues.textPhase1 = $"1. Git обнаружен по пути {gitPath}";
                TextValues.pathToSSHKeygen = Prog.ReturnPath(pathGitSearch, "ssh-agent.exe", "ssh-keygen.exe", 1);
                Prog.UdpateUserConfig("config.tlsm", gitPath, txtbox_git_email.Text, txtbox_path.Text);
                txtbox_path_git_instal.Text = gitPath;
            }
            else
            {
                TextValues.textPhase1 = $"1. Git не обнаружен в {pathGitSearch}\nНеобходимо указать путь к Git или установить его";
            }

            UpdateStatusAllPhases();
        }

        private void btn_copySSHKey_Button_Click(object sender, RoutedEventArgs e)
        {
            string pathSSHKeySearch = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\.ssh";
            string SSHPath = Prog.ReturnPath(pathSSHKeySearch, "id_rsa.pub", "id_rsa", 1);
            if (SSHPath.Length > 0)
            {
                FileStream file = new FileStream(SSHPath + @"\id_rsa.pub", FileMode.Open);
                StreamReader reader = new StreamReader(file);
                string publicKey = reader.ReadToEnd();
                reader.Close();

                if (publicKey.Contains(txtbox_git_email.Text))
                {
                    TextValues.textPhase2 = $"2. SSH-Key обнаружен по пути {SSHPath}\nПубличный ключ:\n{publicKey}Публичный ключ скопирован в буфер обмена.";
                    Clipboard.SetText(publicKey);
                }
                else
                {
                    TextValues.textPhase2 = $"2. SSH-Key обнаружен по пути {SSHPath}, но он не подходит";
                }

            }
            else
            {
                TextValues.textPhase2 = $"2. SSH-Key не обнаружен в {pathSSHKeySearch}\nНеобходимо сгенерировать SSH-Key";
            }


            UpdateStatusAllPhases();
        }

        void UpdateStatusAllPhases()
        {
            txt_status_phase1.Text = TextValues.textPhase1;
            txt_status_phase2.Text = TextValues.textPhase2;
            txt_status_phase3.Text = TextValues.textPhase3;
        }
    }

    public class Cmd
    {
        public static void RunCMD(string path, string name, List<string> commands, bool copyBeforeExec)
        {
            string bat = path + $"\\{name}.bat";

            //Если директории нет, то выход
            if (!Directory.Exists(path))
            {
                return;
            }

            //если БАТник уже есть, то удаляем его
            FileInfo file = new FileInfo(path);
            if (File.Exists(path))
            {
                file.Delete();
            }

            //записываем в батник построчно текст из списка строк и запускаем 
            using (StreamWriter sw = new StreamWriter(bat))
            {
                foreach (var str in commands)
                {
                    sw.WriteLine(str);
                }
            }
            //Process.Start("cmd.exe ", "/c " + bat);

            //если стоит флаг копировать, то перед выполнением копируем файл и назначаем новый путь к батнику для выполнения
            if (copyBeforeExec)
            {
                DirectoryInfo dirInf = new DirectoryInfo(Winforms.Application.StartupPath.ToString());

                string startUpDisc = dirInf.Root.ToString();
                if (Directory.Exists(startUpDisc))
                {
                    FileInfo fiCopy = new FileInfo(bat);
                    bat = startUpDisc + $"{name}.bat"; // менем путь
                    fiCopy.CopyTo(bat);
                }               
            }


            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + bat);
            processInfo.UseShellExecute = false;
            processInfo.CreateNoWindow = true;
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            var process = Process.Start(processInfo);
            process.WaitForExit();

            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            //если стоит флаг копировать, то после выполнения удаляем файл и возвращаем старый путь
            if (copyBeforeExec)
            {
                if (File.Exists(bat))
                {
                    FileInfo fiCopy = new FileInfo(bat);
                    fiCopy.Delete();
                }
                bat = path + $"\\{name}.bat"; // менем путь обратно
            }

            string summaryOutput = "";
            if (name == "Git-Pull-Clone")
            {
                if (Convert.ToInt32(process.ExitCode.ToString()) == 0 && error.Contains("FETCH_HEAD") && error.Contains("HEAD"))
                {
                    if (error.Contains("FETCH_HEAD") && error.Contains("HEAD"))
                    {
                        summaryOutput = "выгрузка успешно завершена";
                        MessageBox.Show(summaryOutput);
                    }
                    else
                    {
                        summaryOutput = "Git-Pull-Clone.bat запущен на выполнение, но загрузка данных не произошла. Пожалуйста, запустите его вручную.";
                        MessageBox.Show(summaryOutput);

                        Prog.SelectFileUsingExplorer(bat);
                    }

                }
                else
                {
                    summaryOutput += "Output: " + (String.IsNullOrEmpty(output) ? "none" : output) + "\n";
                    summaryOutput += "Error: " + (String.IsNullOrEmpty(error) ? "none" : error) + "\n";
                    summaryOutput += "ExitCode: " + process.ExitCode.ToString() + "\n";
                    summaryOutput += "Выполнить Git-Pull-Clone.bat не получилось. Пожалуйста, запустите его вручную.";
                    MessageBox.Show(summaryOutput);

                    Prog.SelectFileUsingExplorer(bat);
                }
            }
            else
            {
                if(Convert.ToInt32(process.ExitCode.ToString()) == 0)
                {
                    summaryOutput = "Создание ключей запущено на выполнение.";
                    MessageBox.Show(summaryOutput);
                }
                else
                {
                    summaryOutput = "Что-то пошло не так. Пожалуйста, запустите .bat вручную.";
                    MessageBox.Show(summaryOutput);

                    Prog.SelectFileUsingExplorer(bat);
                }
            }
            
            
            process.Close();

        }
    }

    public class Prog
    {
        public static string ReturnPath(string directory, string softName1, string softName2, int level)
        {
            string result = "";

            List<string> res = new List<string>();

            var dirs = new Stack<string>();
            dirs.Push(directory);

            while (dirs.Count > 0)
            {
                string currentDirPath = dirs.Pop();

                try
                {
                    string[] subDirs = Directory.GetDirectories(currentDirPath);
                    foreach (var sb in subDirs)
                    {
                        dirs.Push(sb);
                    }

                    string[] allFoundFiles1 = Directory.GetFiles(currentDirPath, softName1);
                    foreach (var st in allFoundFiles1)
                    {
                        res.Add(st);
                    }

                    string[] allFoundFiles2 = Directory.GetFiles(currentDirPath, softName2);
                    foreach (var st in allFoundFiles2)
                    {
                        res.Add(st);
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    continue;
                }
                catch (DirectoryNotFoundException)
                {
                    continue;
                }
            }

            foreach (string str in res)
            {
                for (int i = 0; i < res.IndexOf(str); i++)
                {
                    if (str.Substring(0, str.LastIndexOf('\\')).Equals(res[i].Substring(0, res[i].LastIndexOf('\\'))))
                    {
                        result = str;
                    }
                }
            }
           
            return TrimFullPath(result, level);
        }

        // обрезает полный путь на указанное число уровне, например из B:\Git\usr\bin\ssh-agent.exe c level 3 получается B:\Git
        public static string TrimFullPath(string path, int level)
        {
            if (level == 0)
            {
                return path;
            }
            for (int i = 0; i < level; i++)
            {
                if (path.LastIndexOf('\\') > 0)
                {
                    path = path.Substring(0, path.LastIndexOf('\\'));
                }
            }
            return path;
        }
        
        // менюшка выбора расположения файла
        public static string SelectFolder()
        {
            string path = "";

            Winforms.FolderBrowserDialog dlg = new Winforms.FolderBrowserDialog(); // using WinForms = System.Windows.Forms; , винформс подключил в референсах
            dlg.ShowDialog();        
            path = dlg.SelectedPath;
            return path;
        }

        // читаем пути из файла конфиг
        public static string GetConfig(string configFileName, string varName)
        {
            string gitPath = @"C:\Program Files";
            string bitbucketAccountEmail = "";
            string repoPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\rep\";

            string configLocation = "";

            //если есть в папке файл ЮзерКонфиг, то инфу берем из него
            if (File.Exists(Winforms.Application.StartupPath.ToString() + $"\\User{configFileName}"))
            {
                configLocation = Winforms.Application.StartupPath.ToString() + $"\\User{configFileName}";
            }
            else
            {
                configLocation = Winforms.Application.StartupPath.ToString() + $"\\{configFileName}";
            }
          
            if (File.Exists(configLocation))
            {
                string[] values = File.ReadAllLines(configLocation);
                foreach (string val in values)
                {
                    if (val.Substring(0, val.LastIndexOf('=')).Trim().Equals("gitPath"))
                    {
                        if (val.Length - val.LastIndexOf('=') > 1)
                        {
                            gitPath = val.Substring(val.LastIndexOf('=') + 1, val.Length - val.LastIndexOf('=') - 1).Trim();
                        }
                    }
                    if (val.Substring(0, val.LastIndexOf('=')).Trim().Equals("bitbucketAccountEmail"))
                    {
                        if (val.Length - val.LastIndexOf('=') > 1)
                        {
                            bitbucketAccountEmail = val.Substring(val.LastIndexOf('=') + 1, val.Length - val.LastIndexOf('=') - 1).Trim();
                        }
                    }
                    if (val.Substring(0, val.LastIndexOf('=')).Trim().Equals("repoPath"))
                    {
                        if (val.Length - val.LastIndexOf('=') > 1)
                        {
                            repoPath = val.Substring(val.LastIndexOf('=') + 1, val.Length - val.LastIndexOf('=') - 1).Trim();
                        }
                    }
                }
            }

            switch (varName)
            {
                case "gitPath":
                    return gitPath;
                case "bitbucketAccountEmail":
                    return bitbucketAccountEmail;
                case "repoPath":
                    return repoPath;
            }
            return "";
        }

        //добавляем пути в файл ЮзерКонфиг
        public static void UdpateUserConfig(string configFileName, string gitPathValue, string bitbucketAccountEmailValue, string repoPathValue)
        {

            string userConfigLocation = Winforms.Application.StartupPath.ToString() + $"\\User{configFileName}";

            string[] values =
                             {
                                 "gitPath = " + gitPathValue,
                                 "bitbucketAccountEmail = " + bitbucketAccountEmailValue,
                                 "repoPath = " + repoPathValue
                             };

            File.WriteAllLines(userConfigLocation, values);
        }

        public static void SelectFileUsingExplorer(string path)
        {
            Process pr = new Process();
            ProcessStartInfo prInf = new ProcessStartInfo();
            prInf.CreateNoWindow = true;
            prInf.WindowStyle = ProcessWindowStyle.Normal;
            prInf.FileName = "explorer";
            prInf.Arguments = @"/n, /select, " + path;
            pr.StartInfo = prInf;
            pr.Start();
        }
    }




    public class OSBitCheck
    {
        public static bool Is64()
        {
            bool is64bit = Is64Bit();
            if (is64bit)
                return true;
            else
                return false;
        }

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWow64Process([In] IntPtr hProcess, [Out] out bool lpSystemInfo);

        public static bool Is64Bit()
        {
            bool retVal;
            IsWow64Process(Process.GetCurrentProcess().Handle, out retVal);
            return retVal;
        }
    }

    public class TextValues
    {
        public static string textPhase1 { get; set; }
        public static string textPhase2 { get; set; }
        public static string textPhase3 { get; set; }
        public static string pathToSSHKeygen { get; set; }
    }
}
